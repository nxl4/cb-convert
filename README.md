# CB Convert

## General Description

CB Convert utility is an automated utility that converts ZIP and RAR files into their respective comic book reader formats, CBZ and CBR, for use with comic book readers (e.g. [MComix](https://sourceforge.net/projects/mcomix/)).

## Input

The utility will accept as input either a single archive file, or a directory. If anything other than a validly structured archive file or a directory is input, an error message will be the only output. If a directory is input, the utility will discover any valid archive files contained within, and will iterate through a compiled list of these files. Whether or not this search is recursive depends on optional arguments passed to the script.

### Arguments

There is one positional argument, and two possible optional arguments for the utility:

#### Positional Arguments

The sole required positional argument for the utility is an input name. As indicated above, this can be either a file or a directory. Either relative or absolute paths can be used to define this.

#### Optional Arguments

The two optional arguments are as follows:

* `-h, --help` This argument will print the help documentation to the command prompt.
* `-r, --recursive` This argument will cause the utility to search through directories recursively, identifying any archive files in any subdirectories which are beneath the input directory.

## Usage

The utility can be run from the command line with this general structure:

```shell
python cb-convert.py [-h] [-r] input_name
```

An example of the utility being used to convert a single archive file would be:

```shell
python cb-convert.py ../../file_name.zip
```

An example of the utility being used to recursively search through a directory tree, converting any located archive files in place would be:

```shell
python cb-convert.py -r ../../directory_name/
```
## Author

nxl4: [https://gitlab.com/nxl4](https://gitlab.com/nxl4)

## License

[GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html)