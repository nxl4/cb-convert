PREFIX ?= /usr

all:
	@echo Run \'make install\' to install CB Convert.

install:
	@mkdir -p $(DESTDIR)$(PREFIX)/bin
	@cp -p cb-convert.py $(DESTDIR)$(PREFIX)/bin/cb-convert
	@chmod 755 $(DESTDIR)$(PREFIX)/bin/cb-convert

uninstall:
	@rm -rf $(DESTDIR)$(PREFIX)/bin/cb-convert
