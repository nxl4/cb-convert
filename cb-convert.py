#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""CB Convert

CB Convert utility is an automated utility that converts ZIP 
and RAR files into their respective comic book reader formats, 
CBZ and CBR, for use with comic book readers (e.g. MComix).

Date: 
    27 September 2018

Version: 
    1.0

License:
    GPL v3.0

Authors: 
    nxl4@protonmail.com
"""

import argparse
import os

import magic

def arg_parser():
    """Defines argument parser for CLI script

    Args:
        The parser has one required positional argument, and 
        one optional argument (as well as the default help 
        arguments). The required positional argument is the 
        input file or directory to convert. The optional 
        argument is a recursive option for directory scans.

    Returns:
        args: A list of arguments.
    """
    parser = argparse.ArgumentParser(
        description = ('CB Convert 1.0 '
            '(https://gitlab.com/nxl4/cb-convert): '
            'An automated utility for converting ZIP and RAR '
            'files into CBZ and CBR files respectively.') 
        )
    input_help = 'input path and name, can be either a ' + \
        'single file or a directory containing files'
    parser.add_argument(
        action = 'store',
        nargs = 1,
        type = str,
        help = input_help,
        dest = 'input_name'
        )
    recursive_help = 'for directory inputs, this option ' + \
        'will recursively search for any subdirectories ' + \
        'existing within the root directory'
    parser.add_argument(
        '-r',
        '--recursive',
        action = 'store_true',
        help = recursive_help,
        dest = 'recursive_flag',
        default = False
        )
    args = parser.parse_args()
    return args


def test_input_type(input_name):
    """Tests if input value is a file or directory

    The function first test if the input value is a file, and
    then tests if it is a directory. If either test is true, 
    the type (i.e. file or directory) is stored and passed as 
    the function's output. If both the file and directory tests
    fail, then the type is returned as invalid.

    Args:
        input_name: The file or directory name captured as the
        sole positional argument.

    Returns:
        input_type: A string containing one of three input 
        types: (1) File, (2) Directory, or (3) Invalid.
    """
    is_file = os.path.isfile(input_name)
    is_dir = os.path.isdir(input_name)
    if is_file is True:
        input_type = 'File'
        return input_type
    elif is_dir is True:
        input_type = 'Directory'
        return input_type
    else:
        input_type = 'Invalid'
        return input_type


def test_file_type(file_name):
    """Tests file to determine archive type

    Uses the magic library to determine the type of the
    input file passed in the required positional argument.
    The output of the function either a string, 'ZIP' or 'RAR',
    if the file is one of the respective archive types, or False
    if the file is not an archive.

    Args:
        file_name: The name of the file being tested.

    Returns:
        archive_type: If the file is an archive, this returns
        the type, 'ZIP' or 'RAR'. If it is not an archive, it
        returns False.
    """
    file_type = magic.from_file(file_name)
    if 'ZIP ARCHIVE' in file_type.upper():
        archive_type = 'ZIP'
    elif 'RAR ARCHIVE' in file_type.upper():
        archive_type = 'RAR'
    else:
        archive_type = False
    return archive_type


def input_handler(input_name, file_list, recursive_flag):
    """Handles both file and directory inputs

    Gers type of main input, using test_input_type() function,
    yielding either a 'File' or 'Directory' value. 

    If the input is a file, the absolute path is passed 
    through the test_file_type() function. ZIP and RAR files 
    are appended to the file_list, and a message is printed 
    to the console. Non-archive files are not added, and an 
    error message is printed to the console.

    For directory inputs, a separate file_dir list variable
    is created to store files found in the directory. Each
    object in this new list is tested to see if it is a ZIP
    or RAR. Archive files are added to the main file_list,
    and a message is printed to the console. Non-archive file
    objects are not added, and an error is printed. 

    If the object in the directory is iteself a sub-directory, 
    then the input_handler() function is recursively called.
    This recursive process continues until all testing options
    are exhausted, and the whole directory tree has been
    handled.

    Args:
        input_name: Positional argument, either file directory.
        file_list: List of non-directory file name.
        recursive_flag: Optional argument, True or False.

    Returns:
        file_list: List of any files to be converted.

        If a file is input, the file is tested to 
        determine if it is an archive. If so, the full path 
        of the file is stored in a list. If a directory is 
        input, each file in the directory is tested to 
        determine if it is an archive. If a file is an 
        archive, it is appended to the list. If no archive 
        files are located, in either case, then an empty 
        list is returned.
    """
    main_type = test_input_type(input_name)
    if main_type == 'File':
        file_path = os.path.abspath(input_name)
        main_archive = test_file_type(file_path)
        if main_archive in ['ZIP', 'RAR']:
            file_list.append(file_path)
            print('[+] {0} is a {1} file'.
                format(file_path, main_archive))
        elif main_archive is False:
            print('[-] {0} is not an archive file'.
                format(file_path))
    elif main_type == 'Directory':
        file_dir = []
        for short_file in os.listdir(input_name):
            short_path = os.path.abspath(input_name)
            short_name = os.path.join(short_path, short_file)
            file_dir.append(short_name)
        for file_name in file_dir:
            sub_type = test_input_type(file_name)
            if sub_type == 'File':
                main_archive = test_file_type(file_name)
                if main_archive in ['ZIP', 'RAR']:
                    file_list.append(file_name)
                    print('[+] {0} is a {1} file'.
                        format(file_name, main_archive))
                elif main_archive is False:
                    print('[-] {0} is not an archive file'.
                        format(file_name))
            elif sub_type == 'Directory' and \
                 recursive_flag is True:
                input_handler(file_name, 
                              file_list, 
                              recursive_flag)
    elif main_type == 'Invalid':
        print('[-] {0} is not a valid file or directory name'.
            format(input_name))
    return file_list


def convert(archive_list):
    """Converts archive files

    Function to convert ZIP files into CBZ files, and RAR
    files into CBR files. The function iterates over all archive
    files in the input list variable. If the file is a ZIP or
    RAR, and does not have the proper (CBZ or CBR) extension, 
    the extension is changed. If the file is already either
    a CBZ or CBR, then an error message is printed. For 
    successfully converted files, a success message is printed.

    Args:
        archive_list: This is the file_list returned by the
        input_handler() function.

    Exceptions:
        The handler catches general exceptions causing the
        conversion of ZIP or RAR files to fail. None have
        yet been observed in testing.
    """
    for archive in archive_list:
        archive_tuple = os.path.splitext(archive)
        if test_file_type(archive) == 'ZIP':
            new_archive = archive_tuple[0] + '.cbz'
        elif test_file_type(archive) == 'RAR':
            new_archive = archive_tuple[0] + '.cbr'
        if archive == new_archive:
            print('[-] {0} already formatted correctly, '
                  'nothing to convert'.format(archive))
        else:
            try:
                os.rename(archive, new_archive)
                print('[+] Conversion of {0} to {1} succeeded'.
                    format(archive, 
                           new_archive))
            except Exception as e:
                print('[-] Conversion of {0} to {1} failed: {2}'.
                    format(archive, 
                           new_archive, 
                           str(e)))


def main():
    """Main function that runs conversion operations

    Accepts arguments from the parser, validates the and 
    tests the arguments, inserving False values for the 
    input_name and recursive_flag variables if necessary.
    The arguments are then passed into dependent functions
    to convert files. 

    If no archive files are found, an error message is 
    printed to the console. 

    If a directory with no archive files is input with 
    no recursive option, an error message is printed. 

    If the input value is an archive file, or the directory 
    (or directory tree if the recursive option is used) 
    contains archive files then the convert() function is 
    called on the archive_list.
    
    """
    args = arg_parser()
    try:
        input_name = args.input_name[0]
    except:
        input_name = False
    try:
        recursive_flag = args.recursive_flag
    except:
        recursive_flag = False
    file_list = []
    archive_list = input_handler(input_name,
                                 file_list,
                                 recursive_flag)
    if len(archive_list) == 0 and recursive_flag is False:
        print('[-] Non-recursive query found no archive files, '
              'try again with recursive argument if target '
              'contains subdirectories.')
    elif len(archive_list) == 0 and recursive_flag is not False:
        print('[-] Recursive query found no archive files.')
    else:
        convert(archive_list)


if __name__ == '__main__':
    main()
